package id.co.iconpln.controlflowapp

import android.content.Intent
import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_intent_with_result.*

class IntentWithResultActivity : AppCompatActivity(), View.OnClickListener {

    companion object{
        const val TAG = "intentMoveResult"
        const val EXTRA_VALUE = "extra_value"
        const val RESULT_CODE = 110
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_with_result)

        onClickListener()
    }

    private fun onClickListener() {
        btnResultChoose.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btnResultChoose -> {
                if (rgNumber.checkedRadioButtonId != 0){
                    var value = 0
                    when (rgNumber.checkedRadioButtonId){
                        R.id.rb50 -> value =  50
                        R.id.rb100 -> value = 100
                        R.id.rb150 -> value = 150
                        R.id.rb200 -> value = 200
                    }
                    Log.d(TAG, "Value $value")
                    val resultIntent = Intent()
                    resultIntent.putExtra(EXTRA_VALUE, value) //carrying data
                    setResult(RESULT_CODE, resultIntent) //add back
                    finish()
                }
            }
        }
    }
}
