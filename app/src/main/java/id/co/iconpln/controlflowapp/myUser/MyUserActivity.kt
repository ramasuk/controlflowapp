package id.co.iconpln.controlflowapp.myUser

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity
import kotlinx.android.synthetic.main.activity_my_user.*

class MyUserActivity : AppCompatActivity(), View. OnClickListener  {

    private lateinit var adapter: MyUserAdapter
    private lateinit var myUserViewModel: MyUserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user)

        initViewModel()
        showListMyUser()

        fetchMyUserData()
        addListClickListener()

        onClickListener()
    }

    override fun onResume() {
        super.onResume()
        fetchMyUserData()
    }

    private fun fetchMyUserData() {
        myUserViewModel.getListUsers().observe(this, Observer { myUserItem ->
            if (myUserItem != null){
                adapter.setData(myUserItem)

            }
        })
    }

    private fun initViewModel() {
        myUserViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(MyUserViewModel::class.java)
    }

    private fun showListMyUser() {
        adapter = MyUserAdapter()
        adapter.notifyDataSetChanged()

        rvMyUserList.layoutManager = LinearLayoutManager(this)
        rvMyUserList.adapter = adapter

    }

    private fun addListClickListener(){
        adapter.setOnItemClickCallback(object  : MyUserAdapter.OnItemClickCallBack {
            override fun onItemClick(myUser: UserDataResponse) {
               // Toast.makeText(this@MyUserActivity, myUser.name, Toast.LENGTH_SHORT).show()

                val itemClickIntent = Intent(this@MyUserActivity, MyUserFormActivity::class.java)
                //itemClickIntent.putExtra(MyUserFormActivity.EXTRA_USER, myUser)
                itemClickIntent.putExtra(MyUserFormActivity.EXTRA_USER_ID, myUser.id)
                itemClickIntent.putExtra(MyUserFormActivity.EXTRA_USER_EDIT, true)
                startActivity(itemClickIntent)
            }

        })
    }

    private fun onClickListener(){
        fabMyUserAdd.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.fabMyUserAdd ->{
                val myUserFormIntent = Intent(this, MyUserFormActivity::class.java)
                startActivity(myUserFormIntent)
            }

        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_my_user_favorite_list, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_favorite_list ->{
                val favoriteIntent = Intent(this, MyUserFavoriteActivity::class.java)
                startActivity(favoriteIntent)
            }
        }
        return super.onOptionsItemSelected(item)
    }





}
