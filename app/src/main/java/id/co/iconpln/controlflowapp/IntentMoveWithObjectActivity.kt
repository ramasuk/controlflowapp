package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent_move_with_object.*

class IntentMoveWithObjectActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_PERSON =  "extra_person"
    }

    private lateinit var person : Person

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_move_with_object)

        getIntentExtras()
        showData()
    }


    private fun getIntentExtras() {
        person = intent.getParcelableExtra(EXTRA_PERSON)
    }

    private fun showData() {
        val text = "name ${person.name} \n age ${person.age} \n email ${person.email} \n city ${person.city}"
        tvObject.text = text
    }
}
