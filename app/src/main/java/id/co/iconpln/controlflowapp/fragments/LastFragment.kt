package id.co.iconpln.controlflowapp.fragments


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.co.iconpln.controlflowapp.StyleActivity

import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.fragment_last.*
import kotlinx.android.synthetic.main.fragment_other.*

/**
 * A simple [Fragment] subclass.
 */
class LastFragment : Fragment(), View.OnClickListener {

    companion object{
        const val EXTRA_NAME_FRAGMENT = "extra_name_fragment"
        var message: String = ""
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_last, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val name = arguments?.getString(EXTRA_NAME_FRAGMENT)
        tvFrName.text = name
        tvFrMessage.text = message
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnOtherActivity.setOnClickListener(this)
        btnDialog.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btnOtherActivity -> {
                val lastFragmentIntent = Intent(requireContext(), StyleActivity::class.java)
                startActivity(lastFragmentIntent)
            }
            R.id.btnDialog -> {
                val fragmentManager = childFragmentManager
                val optionDialogFragment = OptionDialogFragment()
                optionDialogFragment.show(fragmentManager, OptionDialogFragment::class.java.simpleName)
            }
        }

    }

    var optionDialogListener: OptionDialogFragment.OnOptionDialogListener =
        object : OptionDialogFragment.OnOptionDialogListener{
            override fun onOptionChosen(text: String) {
                Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show()
            }
        }


}
