package id.co.iconpln.controlflowapp.model.myUser

data class CreateUserResponse<T>(
    val created_users: UserDataResponse,
    val message: String
)