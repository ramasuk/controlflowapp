package id.co.iconpln.controlflowapp.myUser

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import kotlinx.android.synthetic.main.item_list_my_user.view.*

class MyUserAdapter: RecyclerView.Adapter<MyUserAdapter.MyUserViewHolder>() {

    private val userData = ArrayList<UserDataResponse>()
    private lateinit var onItemClickCallBack: OnItemClickCallBack

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyUserViewHolder {
       val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_list_my_user, parent, false)
        return MyUserViewHolder((mView))
    }

    override fun getItemCount(): Int {
        return userData.size
    }

    override fun onBindViewHolder(holder: MyUserViewHolder, position: Int) {
        holder.bind(userData[position])
    }

    fun setData(myUserItem: List<UserDataResponse>){
        userData.clear()
        userData.addAll(myUserItem)
        notifyDataSetChanged()
    }

    inner class MyUserViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(myUserItem: UserDataResponse){
            itemView.tvUserName.text = myUserItem.name
            itemView.tvUserAdress.text = myUserItem.address
            itemView.tvUserMobile.text = myUserItem.phone
            itemView.setOnClickListener {
                onItemClickCallBack.onItemClick(myUserItem)
            }
        }
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallBack){
        this.onItemClickCallBack = onItemClickCallback
    }

    interface OnItemClickCallBack {
        fun onItemClick(myUser: UserDataResponse)
    }

}