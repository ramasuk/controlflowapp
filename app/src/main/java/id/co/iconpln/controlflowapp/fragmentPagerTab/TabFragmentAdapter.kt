package id.co.iconpln.controlflowapp.fragmentPagerTab

import android.content.Context
import androidx.annotation.Nullable
import androidx.annotation.StyleRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.hero.ListHeroFragment

class TabFragmentAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm){

    @StyleRes
    private val TAB_TITLES = intArrayOf(R.string.tab_text_1, R.string.tab_text_2)

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when(position){
            0 -> fragment = OneFragment()
            1 -> fragment = ListHeroFragment()
        }
        return fragment as Fragment
    }

    override fun getCount(): Int {
        return 2
    }

    @Nullable
    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
        //return super.getPageTitle(position)
    }


}
