package id.co.iconpln.controlflowapp

import android.graphics.Path
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_operation.*
import java.text.DecimalFormat

class OperationActivity : AppCompatActivity(), View.OnClickListener {

    private var inputX: Double = 0.0
    private var inputY: Double = 0.0

    private lateinit var operationViewModel : OperationViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operation)

        initViewModel()
        displayResult()
        setButtonClickListener()


    }

    private fun displayResult() {
        tvCalculateResult.text = operationViewModel.operationResult.toString()
        tvOperation.text = operationViewModel.operator
    }

    private fun initViewModel() {
        operationViewModel = ViewModelProviders.of(this).get(OperationViewModel::class.java)
    }



    private fun getInputNumbers() {

        if (etBilanganX.text?.isNotEmpty() == true || etBilanganY.text?.isNotEmpty() == true) {

                inputX = etBilanganX.text.toString().toDouble()
                inputY = etBilanganY.text.toString().toDouble()
            }
    }


    private fun setButtonClickListener() {
        btnOpAdd.setOnClickListener(this)
        btnOpDivide.setOnClickListener(this)
        btnOpMultiply.setOnClickListener(this)
        btnOpSub.setOnClickListener(this)
        btnCalculate.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnOpAdd -> {
                getInputNumbers()
                //tvOperation.text = getSring(R.string.tvOperateAdd)
                operationViewModel.operator = getString(R.string.tvOperateAdd)
                val penjumlahan = Operation.Add(inputX)
                //operationResult = (execute(inputY, penjumlahan)).toString().toDouble()
                operationViewModel.execute(inputY, penjumlahan)
                displayResult()

            }
            R.id.btnOpDivide -> {
                getInputNumbers()
                //tvOperation.text = getString(R.string.tvOperateDivide)
                operationViewModel.operator = getString(R.string.tvOperateDivide)
                val bagi = Operation.Divide(inputX)
                //operationResult = (execute(inputY, bagi)).toString().toDouble()
                operationViewModel.execute(inputY, bagi)
                displayResult()
            }
            R.id.btnOpMultiply -> {
                getInputNumbers()
                //tvOperation.text = getString(R.string.tvOperateMultiply)
                operationViewModel.operator = getString(R.string.tvOperateMultiply)
                val kali = Operation.Multiply(inputX)
               // operationResult = (execute(inputY, kali)).toString().toDouble()
                operationViewModel.execute(inputY, kali)
                displayResult()
            }
            R.id.btnOpSub -> {
                getInputNumbers()
                //tvOperation.text = getString(R.string.tvOperateKurang)
                operationViewModel.operator = getString(R.string.tvOperateKurang)
                val kurang = Operation.Substract(inputX)
                //operationResult = (execute(inputY, kurang)).toString().toDouble()
                operationViewModel.execute(inputY, kurang)
                displayResult()
            }
            R.id.btnCalculate -> {
                getInputNumbers()
                etBilanganX.setText(R.string.tvZero)
                etBilanganY.setText(R.string.tvZero)
                operationViewModel.operationResult = 0.0

                tvOperation.text = getString(R.string.tvZero)
                displayResult()
            }
        }
    }

}
