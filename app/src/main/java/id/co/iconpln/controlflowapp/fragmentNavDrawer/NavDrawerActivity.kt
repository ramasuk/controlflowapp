package id.co.iconpln.controlflowapp.fragmentNavDrawer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.StyleActivity
import id.co.iconpln.controlflowapp.fragmentTab.FirstFragment
import id.co.iconpln.controlflowapp.fragmentTab.SecondFragment
import id.co.iconpln.controlflowapp.hero.ListHeroFragment
import kotlinx.android.synthetic.main.activity_nav_drawer.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class NavDrawerActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_drawer)

        setupActionBar()
        navViewDrawer.setNavigationItemSelectedListener(this)
        selectFirstNavigationMenu()
    }

    private fun selectFirstNavigationMenu() {
        navViewDrawer.menu.performIdentifierAction(R.id.nav_home, 0)
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(this, dlDRawerLayout, toolbar, R.string.app_name, 0)
        dlDRawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_home -> {
                loadFragment(FirstFragment())
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_music -> {
                loadFragment(ListHeroFragment())
                Toast.makeText(this, "Music", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_gallery -> {
                val openActivityIntent = Intent(this, StyleActivity::class.java)
                startActivity(openActivityIntent)
                Toast.makeText(this, "gallery", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_edit -> {
                loadFragment(SecondFragment())
            }
            R.id.nav_exit -> {
                finish()
            }
        }

        uncheckedMenu()
        item.isChecked = true
        title = item.title

        dlDRawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun loadFragment(fragment: Fragment){
        supportFragmentManager.beginTransaction()
            .replace(R.id.flDrawerContent, fragment, fragment::class.java.simpleName)
            .commit()

    }

    private fun uncheckedMenu(){
        //for uncheck when clicked
        for (menuCount in 0 until navViewDrawer.menu.size()){
            navViewDrawer.menu.getItem(menuCount).isChecked = false

            //checking sub menu uncheck
            if (navViewDrawer.menu.getItem(menuCount).hasSubMenu()){
                for (subMenuCount in 0 until navViewDrawer.menu.getItem(menuCount).subMenu.size()){
                    navViewDrawer.menu.getItem(menuCount).subMenu.getItem(subMenuCount).isChecked = false
                }
            }

        }

    }

}
