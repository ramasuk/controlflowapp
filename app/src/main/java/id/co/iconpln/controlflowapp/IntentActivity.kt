package id.co.iconpln.controlflowapp

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent.*

class IntentActivity : AppCompatActivity(), View.OnClickListener {

    private val REQUEST_CODE = 110

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent)

        setOnClickButton()
    }

    private fun setOnClickButton(){
        btnIntent_move_activity.setOnClickListener(this)
        btnIntent_move_activity_with_data.setOnClickListener(this)
        btnIntent_move_activity_with_bundle.setOnClickListener(this)
        btnIntent_move_activity_with_object.setOnClickListener(this)
        btnIntent_with_result.setOnClickListener(this)
        btnIntent_implicit.setOnClickListener(this)
        btnIntent_open_web.setOnClickListener(this)
        btnIntent_send_sms.setOnClickListener(this)
        btnIntent_show_map.setOnClickListener(this)
        btnIntent_share_text.setOnClickListener(this)

    }

    override fun onClick(v: View) {
        when (v.id){
            R.id.btnIntent_move_activity -> {
                val moveActivity = Intent(this, StyleActivity::class.java)
                startActivity(moveActivity)
            }
            R.id.btnIntent_move_activity_with_data -> {
                val moveWithDataIntent = Intent(this, IntentMoveWithDataActivity::class.java)
                moveWithDataIntent.putExtra(IntentMoveWithDataActivity.EXTRA_NAME, "ramasuk")
                moveWithDataIntent.putExtra(IntentMoveWithDataActivity.EXTRA_AGE, 20)
                startActivity(moveWithDataIntent)
            }
            R.id.btnIntent_move_activity_with_bundle -> {
                val moveWithBundleIntent = Intent(this, IntentMoveWithBundleActivity::class.java)
                val bundle = Bundle()

                bundle.putString(IntentMoveWithBundleActivity.EXTRA_BUNDLE_NAME, "ramasuk")
                bundle.putInt(IntentMoveWithBundleActivity.EXTRA_BUNDLE_AGE, 20)
                moveWithBundleIntent.putExtras(bundle)
                startActivity(moveWithBundleIntent)
            }
            R.id.btnIntent_move_activity_with_object -> {
                val person = Person("ramasuk", 20, "raaamaaa10@gmail.com", "YK")
                val moveWithObjectActivity = Intent(this, IntentMoveWithObjectActivity::class.java)
                moveWithObjectActivity.putExtra(IntentMoveWithObjectActivity.EXTRA_PERSON, person)
                startActivity(moveWithObjectActivity)
            }
            R.id.btnIntent_implicit -> {
                val phoneNumber = "082324721232"
                val dialPhoneNUmber = Intent(Intent.ACTION_DIAL, Uri.parse("tel: $phoneNumber"))

                if (dialPhoneNUmber.resolveActivity(packageManager) != null){
                    startActivity(dialPhoneNUmber)
                }

            }
            R.id.btnIntent_with_result -> {
                val withResultIntent = Intent(this, IntentWithResultActivity::class.java)
                startActivityForResult(withResultIntent, REQUEST_CODE)
            }
            R.id.btnIntent_open_web -> {
                val webpage = Uri.parse("https://www.instagram.com")
                val openWebIntent = Intent(Intent.ACTION_VIEW, webpage)

                if (openWebIntent.resolveActivity(packageManager) != null){
                    startActivity(openWebIntent)
                }else{
                    Toast.makeText(this, "please install some browsers",Toast.LENGTH_SHORT).show()
                }

            }
            R.id.btnIntent_send_sms -> {
                val phoneNumber = 14045
                val sendSms = Uri.parse("smsto: $phoneNumber")

                val message = "mau pesan mcd dong"
                val sendSmsIntent = Intent(Intent.ACTION_SENDTO, sendSms)
                sendSmsIntent.putExtra("sms_body", message)

                if (sendSmsIntent.resolveActivity(packageManager) != null){
                    startActivity(sendSmsIntent)
                }
            }
            R.id.btnIntent_show_map -> {
                val latitude = "47.6"
                val longitude = "-110.5"
                val showMap = Uri.parse("geo: $latitude, $longitude")

                val showMapIntent = Intent(Intent.ACTION_VIEW,showMap)

                if (showMapIntent.resolveActivity(packageManager) != null){
                    startActivity(showMapIntent)
                }


            }
            R.id.btnIntent_share_text -> {
                val sharedText = "this text will be shared"
                val shareTextIntent = Intent(Intent.ACTION_SEND)
                shareTextIntent.putExtra(Intent.EXTRA_TEXT, sharedText)
                shareTextIntent.type = "text/plain"

                val shareIntent = Intent.createChooser(shareTextIntent, null)
                startActivity(shareIntent)

            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE){
            if (resultCode == IntentWithResultActivity.RESULT_CODE){
                val selectedValue = data?.getIntExtra(IntentWithResultActivity.EXTRA_VALUE, 0)
                tvIntentResult.text = "$selectedValue"
            }
        }
    }

}
