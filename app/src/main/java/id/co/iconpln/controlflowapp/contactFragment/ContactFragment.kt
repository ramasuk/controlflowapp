package id.co.iconpln.controlflowapp.contactFragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.contact.ContactViewModel

import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.fragment_contact.*

/**
 * A simple [Fragment] subclass.
 */
class ContactFragment : Fragment() {

    private lateinit var adapter: ContactFragmentAdapter

    private lateinit var contactViewModel: ContactViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViewModel()
        contactViewModel.setContact()
        showListContact()
        fetchContactData()
    }

    private fun initViewModel() {
       contactViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(ContactViewModel::class.java)
    }

    private fun showListContact() {
        adapter = ContactFragmentAdapter()
        adapter.notifyDataSetChanged()

        rvContactFragmentList.layoutManager = LinearLayoutManager(requireContext())
        rvContactFragmentList.adapter = adapter
    }

    private fun fetchContactData() {
        //get value from view model's live data
        showLoading(true)
        contactViewModel.getContact().observe(this, Observer { contactItem ->
            if (contactItem != null){
                adapter.setData(contactItem)
                showLoading(false)
            }
        })
    }

    private fun showLoading(state : Boolean) {
        if (state){
            pbContactFragment.visibility = View.VISIBLE
        }else {
            pbContactFragment.visibility = View.GONE
        }
    }


}
