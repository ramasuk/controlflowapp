package id.co.iconpln.controlflowapp.fragmentTab

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.hero.ListHeroFragment
import kotlinx.android.synthetic.main.activity_tab.*

class TabActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView( R.layout.activity_tab)

        setFragment()
    }

    private fun setFragment() {
        val adapter = TabPagerAdapter(supportFragmentManager)
        adapter.addFragment(ListHeroFragment(), "First tab" )
        adapter.addFragment(SecondFragment(), "Second Tab")
        viewPager.adapter = adapter
        tabsLayout.setupWithViewPager(viewPager)
    }
}
