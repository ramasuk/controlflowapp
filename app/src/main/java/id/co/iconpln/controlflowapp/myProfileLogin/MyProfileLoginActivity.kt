package id.co.iconpln.controlflowapp.myProfileLogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myProfile.ProfileLoginResponse
import id.co.iconpln.controlflowapp.model.myProfile.ProfileLoginUser
import id.co.iconpln.controlflowapp.myProfileRegister.MyProfileRegisterActivity
import kotlinx.android.synthetic.main.activity_my_profile_login.*
import kotlinx.android.synthetic.main.activity_my_profile_register.*

class MyProfileLoginActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_PROFILE_RESULT = "extra_profile_result"
        const val RESULT_CODE = 201
    }

    private lateinit var viewModel: MyProfileLoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile_login)

        initViewModel()
        titleBar()
        onClickTextListener()
    }

    private fun titleBar() {
        supportActionBar?.title = "Login"
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            MyProfileLoginViewModel::class.java
        )
    }

    private fun onClickTextListener() {
        tvProfileLoginRegistration.setOnClickListener(this)
        btnProfileLogin.setOnClickListener(this)
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvProfileLoginRegistration -> {
                val loginRegistrationPage = Intent(this, MyProfileRegisterActivity::class.java)
                startActivity(loginRegistrationPage)
            }
            R.id.btnProfileLogin -> {
                fetchData(
                    ProfileLoginUser(
                        etProfileLoginEmail.text.toString(),
                        etProfileLoginPassword.text.toString()
                    )
                )
            }

        }
    }

    private fun fetchData(profileLoginUser: ProfileLoginUser) {

        viewModel.login(profileLoginUser).observe(this, Observer { loginResponse ->
            val errorMessage = MyProfileLoginViewModel.errorMessage
            if (loginResponse != null) {
                Toast.makeText(
                    this,
                    "Success Login" + "${loginResponse.customer.email}",
                    Toast.LENGTH_SHORT
                ).show()

                openProfilePage(loginResponse)
            } else {
                if (errorMessage.isNotEmpty()) {
                    Toast.makeText(
                        this,
                        "Login Failed : ${MyProfileLoginViewModel.errorMessage}",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        this,
                        "Check Your Connection",
                        Toast.LENGTH_SHORT
                    ).show()
                }


            }
        })
    }

    private fun openProfilePage(profileLoginResponse: ProfileLoginResponse) {
        val resultIntent = Intent().putExtra(EXTRA_PROFILE_RESULT, profileLoginResponse)
        setResult(RESULT_CODE, resultIntent)
        finish()
    }
}
