package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        //todo 1: username and password tidak boleh empty
        //todo 2. pass min 7 digit
        //todo 3. akan sukses jika login aaa@gmail.com, pass
        //todo 4. username format email

        btnLogin.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnLogin -> checkLogin()
        }
    }

    private fun checkLogin(){
        val username = etLoginUsername.text.toString()
        val password = etLoginPassword.text.toString()
        var message = toString()
        when{
            etLoginUsername.text.isEmpty() -> etLoginUsername.error = "can't be blank"
            etLoginPassword.text.isEmpty() -> etLoginPassword.error = "can't be blank"
            etLoginPassword.text.length < 7 -> etLoginPassword.error = "at least min 7 length"
            !Patterns.EMAIL_ADDRESS.matcher(etLoginUsername.text).matches()  -> etLoginUsername.error = "wrong email format"
            else -> {
                if (username == "user@mail.com" && password == "password"){
                    message = "log in"
                }else{
                    message = "error"
                }
                tvStatus.text = message
            }
        }
    }

//    private fun checkLogin() {
//        tvStatus.text = "status :"
//        if (etLoginUsername.text.isEmpty()) {
//            etLoginUsername.error = "email kok kosong?"
//        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(etLoginUsername.text).matches()) {
//            etLoginUsername.error = "email salah"
//        } else if (etLoginPassword.text.isEmpty()) {
//            etLoginPassword.error = "password kok kosong?"
//        } else if (etLoginPassword.text.length < 7) {
//            etLoginPassword.error = "password harus lebih dari 7"
//        } else {
//            tvStatus.text = "login sukses"
//        }
//    }

}
