package id.co.iconpln.controlflowapp.network

import android.util.Log
import androidx.lifecycle.MutableLiveData
import id.co.iconpln.controlflowapp.model.myProfile.*
import id.co.iconpln.controlflowapp.myProfileLogin.MyProfileLoginViewModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyProfileNetworkRepository {

    fun doLogin(profileLoginUser: ProfileLoginUser):MutableLiveData<ProfileLoginResponse>{
            val loginData = MutableLiveData<ProfileLoginResponse>()

            NetworkConfig.ProfileAPI().loginUser(profileLoginUser).enqueue(object :  Callback<BaseProfileLoginResponse<ProfileLoginResponse>> {
                override fun onFailure(
                    call: Call<BaseProfileLoginResponse<ProfileLoginResponse>>,
                    t: Throwable
                ) {
                    loginData.postValue(null)
                }

                override fun onResponse(
                    call: Call<BaseProfileLoginResponse<ProfileLoginResponse>>,
                    loginResponse: Response<BaseProfileLoginResponse<ProfileLoginResponse>>
                ) {
                    if (loginResponse.isSuccessful){
                        val responseLogin = loginResponse.body()?.data
                        loginData.postValue(responseLogin)
                    }else{
                        when (loginResponse.code()){
                            in 400..420 -> {
                                val errorResponse = JSONObject(loginResponse.errorBody()?.string() ?: "")
                                val errorMessage = errorResponse.getJSONArray("messages")[0].toString()
                                MyProfileLoginViewModel.errorMessage = errorMessage
                                Log.d(
                                    "rhama",
                                    "Error Code : " + loginResponse.code().toString() + " - - " + MyProfileLoginViewModel.errorMessage
                                )
                            }
                            else -> MyProfileLoginViewModel.errorMessage = "Unknown network error"
                        }
                        loginData.postValue(null)
                    }
                }
            })
            return loginData

    }

    fun doRegister(profileRegisterUser : ProfileRegisterUser): MutableLiveData<ProfileResponse>{
        val registerData = MutableLiveData<ProfileResponse>()

        NetworkConfig.ProfileAPI().registerUser(profileRegisterUser).enqueue(object : Callback<ProfileRegisterResponse<ProfileResponse>>{
            override fun onFailure(
                call: Call<ProfileRegisterResponse<ProfileResponse>>,
                t: Throwable
            ) {
                registerData.postValue(null)
            }

            override fun onResponse(
                call: Call<ProfileRegisterResponse<ProfileResponse>>,
                response: Response<ProfileRegisterResponse<ProfileResponse>>
            ) {
                if (response.isSuccessful){
                    val registerResponse = response.body()?.data
                    registerData.postValue(registerResponse)
                }else{
                    registerData.postValue(null)
                }
            }
        })

        return registerData
    }

    fun getProfile(token: String): MutableLiveData<ProfileResponse>{
        val profileData = MutableLiveData<ProfileResponse>()

        NetworkConfig.ProfileAPI().getProfile("Bearer $token").enqueue(object : Callback<BaseProfileResponse>{
            override fun onFailure(call: Call<BaseProfileResponse>, t: Throwable) {

                MyProfileLoginViewModel.errorMessage = ""
                profileData.postValue(null)
            }

            override fun onResponse(
                call: Call<BaseProfileResponse>,
                response: Response<BaseProfileResponse>
            ) {
                if (response.isSuccessful){

                    val profileResponse = response.body()?.data
                    profileData.postValue(profileResponse)
                }
            }
        })

        return profileData
    }
}
