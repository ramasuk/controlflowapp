package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_volume.*

class VolumeActivity : AppCompatActivity() {

//    private var length: Int = 0
//    private var width: Int = 0
//    private var height: Int = 0


    private lateinit var volumeViewModel : VolumeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volume)

       // btnResult.setOnClickListener(this)

        initViewModel()
        displayResult()
        setOnClickListener()
    }

    private fun initViewModel() {
        volumeViewModel = ViewModelProviders.of(this).get(VolumeViewModel::class.java)
    }


//    override fun onClick(v: View) {
//        when (v.id){
//            R.id.btnResult -> checkResult()
//        }
//    }
//
//    private fun checkResult(){
//
//        when{
//            etLength.text.isEmpty() -> etLength.error = "can't be empty"
//            etHeight.text.isEmpty() -> etHeight.error = "can't be empty"
//            etWidth.text.isEmpty() -> etWidth.error = "can't be empty"
//        }
//    }

    private fun displayResult(){
        tvResult.text = volumeViewModel.volumeResult.toString()
    }

    private fun setOnClickListener() {
        btnResult.setOnClickListener {
            val length = etLength.text.toString()
            val width = etWidth.text.toString()
            val height = etHeight.text.toString()

            when {
                length.isEmpty() -> etLength.error = "can't be empty"
                width.isEmpty() -> etWidth.error = "can't be empty"
                height.isEmpty() -> etHeight.error = "can't be empty"
                else -> {
                    volumeViewModel.calculate(length, width, height)
                    displayResult()
                }
            }
        }
    }





}
