package id.co.iconpln.controlflowapp.fragmentPagerTab

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_tab_pager.*

class TabPagerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab_pager)
        setupTab()
    }

    private fun setupTab() {
        val setTabAdapter = TabFragmentAdapter(this, supportFragmentManager)
        vpPager.adapter = setTabAdapter
        tabTab.setupWithViewPager(vpPager)

        supportActionBar?.elevation = 0f
    }
}
