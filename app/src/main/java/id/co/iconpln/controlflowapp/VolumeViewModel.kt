package id.co.iconpln.controlflowapp

import androidx.lifecycle.ViewModel

class VolumeViewModel : ViewModel() {

    var volumeResult: Int = 0

    fun calculate(width: String, length: String, height: String){
        volumeResult = Integer.parseInt(width) * Integer.parseInt(length) * Integer.parseInt(height)
    }

}