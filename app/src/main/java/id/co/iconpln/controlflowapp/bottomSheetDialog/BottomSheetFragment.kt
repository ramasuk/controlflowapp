package id.co.iconpln.controlflowapp.bottomSheetDialog


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.fragment_bottom_sheet.*
import kotlin.system.exitProcess

/**
 * A simple [Fragment] subclass.
 */
class BottomSheetFragment : BottomSheetDialogFragment(), View.OnClickListener {

    private var itemClickListener: ItemClickListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        llBottomPreview.setOnClickListener(this)
        llBottomEdit.setOnClickListener(this)
        llBottomSearch.setOnClickListener(this)
        llBottomExit.setOnClickListener(this)
        llBottomShare.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.llBottomPreview -> {
                if (itemClickListener != null){
                    itemClickListener?.onItemClick(tvBottomPreview.text.toString())
                }
            }
            R.id.llBottomEdit -> {
                if (itemClickListener != null){
                    itemClickListener?.onItemClick(tvBottomEdit.text.toString())
                }
            }
            R.id.llBottomSearch -> {
                if (itemClickListener != null){
                    itemClickListener?.onItemClick(tvBottomSearch.text.toString())
                }
            }
            R.id.llBottomExit -> {
                if (itemClickListener != null){
                    itemClickListener?.onItemClick(tvBottomExit.text.toString())
                }
            }
            R.id.llBottomShare -> {
                val shareTextIntent = "Share this text"
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareTextIntent)
                shareIntent.type = "text/plain"

                val sharedIntent = Intent.createChooser(shareIntent, "share to")
                startActivity(sharedIntent)
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is ItemClickListener){
            this.itemClickListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
    }

    interface ItemClickListener{
        fun onItemClick(text: String)
    }


}
