package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etNilai.setText("0")
        btnShow.setOnClickListener {
            if (etNilai.text.isNotEmpty()) {
                val angka = etNilai.text.toString().toInt()
                hitungPangkat(angka)
            }
        }
    }

    fun hitungPangkat(nilai: Int) {
        val hasil = nilai * nilai
        tvText.text = "hasilnya $hasil"
    }


}

