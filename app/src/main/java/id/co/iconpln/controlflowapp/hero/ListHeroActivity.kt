package id.co.iconpln.controlflowapp.hero

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.Hero
import kotlinx.android.synthetic.main.activity_list_hero.*

class ListHeroActivity : AppCompatActivity() {

    private var listHero: ArrayList<Hero> = arrayListOf()
    private var modeList = true
    private var title: String = "Mode List"
    private var mode: Int = 0

    companion object {
        private const val STATE_TITLE = "state_title"
        private const val STATE_LIST = "state_list"
        private const val STATE_MODE = "state_mode"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_hero)

        setupListHero()

        if (savedInstanceState == null) {
            mode = R.id.actionHeroList
            showRecycleList()
            setActionTitle(title)

        } else{
            title = savedInstanceState.getString(STATE_TITLE).toString()
            val stateList = savedInstanceState.getParcelableArrayList<Hero>(STATE_LIST)
            val stateMode = savedInstanceState.getInt(STATE_MODE)

            setActionTitle(title)
            if (stateList != null){
                listHero.addAll(stateList)
            }
            setListMode(stateMode)
        }
    }

    private fun setActionTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(STATE_TITLE, title)
        outState.putInt(STATE_MODE, mode)
        outState.putParcelableArrayList(STATE_LIST, listHero)
    }


    private fun showRecycleListWithDivider() {
        showRecycleList()
        setupListDivider()
    }

    private fun showRecycleList() {
        rvListHero.layoutManager = LinearLayoutManager(this)
        val listHeroAdapter = ListHeroAdapter(listHero)
        rvListHero.adapter = listHeroAdapter

        listHeroAdapter.setOnItemClickCallBack(object : ListHeroAdapter.OnItemCallBack {
            override fun onItemClick(hero: Hero) {
                Toast.makeText(this@ListHeroActivity, hero.name, Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun setupListHero() {
        rvListHero.setHasFixedSize(true)
        listHero.addAll(getDataHero())
    }

    //add divider
    private fun setupListDivider() {
        val dividerItemDecoration =
            DividerItemDecoration(rvListHero.context, DividerItemDecoration.VERTICAL)
        rvListHero.addItemDecoration(dividerItemDecoration)
    }

    private fun getDataHero(): ArrayList<Hero> {
        val heroName = resources.getStringArray(R.array.hero_name)
        val heroDesc = resources.getStringArray(R.array.hero_description)
        val heroPhoto = resources.getStringArray(R.array.hero_photo)

        val listHero = ArrayList<Hero>()
        for (position in heroName.indices) {
            val hero = Hero(
                heroName[position],
                heroDesc[position],
                heroPhoto[position]
            )
            listHero.add(hero)
        }
        return listHero
    }

    //show recycle grid
    private fun showRecycleGrid() {
        rvListHero.layoutManager = GridLayoutManager(this, 2)
        val gridHeroAdapter = GridHeroAdapter(listHero)
        rvListHero.adapter = gridHeroAdapter

        gridHeroAdapter.setOnItemGridCallBack(object : GridHeroAdapter.OnItemClickCallBack {
            override fun onItemClick(hero: Hero) {
                Toast.makeText(this@ListHeroActivity, hero.name, Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_hero, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        setListMode(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    private fun setListMode(selectedMode: Int) {
        if (selectedMode == R.id.actionHeroList) {
            if (modeList) {
                title = "Mode Grid"
                showRecycleGrid()
                modeList = false
            } else {
                title = "Mode List"
                showRecycleListWithDivider()
                modeList = true
            }

        }
        mode = selectedMode
        setActionTitle(title)


        val changed = if (modeList) "Changed to List" else "Changed to Grid"
        Toast.makeText(this, changed, Toast.LENGTH_SHORT).show()

    }
}
