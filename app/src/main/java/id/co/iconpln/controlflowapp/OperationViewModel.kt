package id.co.iconpln.controlflowapp

import androidx.lifecycle.ViewModel
import java.text.DecimalFormat

class OperationViewModel :ViewModel(){

    var operator: String = ""

     var operationResult: Double = 0.0

    fun execute(x: Double, operation: Operation){
        when (operation) {
            is Operation.Add -> operationResult = operation.value + x
            is Operation.Divide -> operationResult = operation.value / x
            is Operation.Multiply -> operationResult = operation.value * x
            is Operation.Substract -> operationResult = operation.value - x
        }
    }

}