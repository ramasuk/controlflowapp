package id.co.iconpln.controlflowapp.myProfileFirst

import android.content.Context
import id.co.iconpln.controlflowapp.model.myProfile.ProfileUser

internal class ProfileUserPreference(context: Context){
    companion object{
        private const val PREFS_PROFILE_USER = "profile_user_prefs"
        private const val TOKEN = "token"
    }

    private val preference = context.getSharedPreferences(PREFS_PROFILE_USER, Context.MODE_PRIVATE)

    fun setProfileUser(value: ProfileUser){
        val editor = preference.edit()
        editor.putString(TOKEN, value.userToken)
        editor.apply()
    }

    fun getProfileUser(): ProfileUser{
        val model = ProfileUser()
        model.userToken = preference.getString(TOKEN, "")
        return model
    }

    fun removeProfileUser(value: ProfileUser){
        val editor = preference.edit()
        editor.remove(TOKEN)
        editor.apply()
    }

}