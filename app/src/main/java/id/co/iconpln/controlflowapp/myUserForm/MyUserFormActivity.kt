package id.co.iconpln.controlflowapp.myUserForm


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import id.co.iconpln.controlflowapp.database.FavoriteViewModel
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import kotlinx.android.synthetic.main.activity_my_user_form.*

class MyUserFormActivity : AppCompatActivity(), View.OnClickListener{

    companion object{
        const val EXTRA_USER = "extra_user"
        const val EXTRA_USER_EDIT = "extra_user_edit"
        const val EXTRA_USER_ID = "extra_user_id"
    }

    private lateinit var user: UserDataResponse
    private lateinit var myUserFormViewModel: MyUserFormViewModel

    private lateinit var favoriteViewModel: FavoriteViewModel

    private var userId: Int? = null

    private var isEditUser = false
    private var isFavorite: Boolean = false
    private var menuItem: Menu? = null
    private var favoriteUserId: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user_form)

        initViewModel()
        initIntentExtra()

        setOnClickListener()
        checkForm(isEditUser)


    }

    private fun fetchUserData(){
        pbMyUserFormLoading.visibility = View.VISIBLE
        llMyUserFormContent.visibility = View.GONE
        getUser(userId as Int)
    }

    private fun initViewModel() {
        myUserFormViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            MyUserFormViewModel::class.java)

        favoriteViewModel = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application))
            .get(FavoriteViewModel::class.java)
    }

    private fun populateFormData(user: UserDataResponse) {
        etUserFormName.setText(user.name)
        etUserFormAddress.setText(user.address)
        etUserFormHp.setText(user.phone)
       // userId = user.id

        btnUserFormCreate.visibility = View.GONE
        btnUserFormDelete.visibility = View.VISIBLE
        btnUserFormSave.visibility = View.VISIBLE
    }

    private fun initIntentExtra() {
//        if (intent.hasExtra(EXTRA_USER)){
//            user = intent.getParcelableExtra(EXTRA_USER)
//        }else{
//            user = intent.getParcelableExtra(EXTRA_USER)?: UserDataResponse("", -1, "", "")
//        }
        userId = intent.getIntExtra(EXTRA_USER_ID, 0)
        isEditUser = intent.getBooleanExtra(EXTRA_USER_EDIT, false)
    }

    private fun checkForm(editUser: Boolean) {
        if (editUser){
           // populateFormData(user)
            fetchUserData()
        }else{
            btnUserFormSave.visibility = View.GONE
            btnUserFormDelete.visibility = View.GONE
            btnUserFormCreate.visibility = View.VISIBLE
        }
    }


    private fun setOnClickListener() {
        btnUserFormSave.setOnClickListener(this)
        btnUserFormDelete.setOnClickListener(this)
        btnUserFormCreate.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id){
            R.id.btnUserFormSave -> {
                if (userId != null){
                    val updateUserData = UserDataResponse(
                        etUserFormAddress.text.toString(), userId ?: 0,
                        etUserFormName.text.toString(),
                        etUserFormHp.text.toString()
                        )

                    updateUser(userId as Int, updateUserData)
                }
            }
            R.id.btnUserFormDelete ->{
                if (userId != null){
                    deleteUser(userId as Int)
                }

            }
            R.id.btnUserFormCreate-> {
                userId = 0
                    val createUserData = UserDataResponse(
                        etUserFormAddress.text.toString(),
                        userId?: 0,
                        etUserFormName.text.toString(),
                        etUserFormHp.text.toString()
                    )
                    createUser(createUserData)

            }
        }
    }

    private fun createUser(userData: UserDataResponse) {
        showLoading(false)
        myUserFormViewModel.createUser(userData).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null){
                Toast.makeText(this, "Successfully Created", Toast.LENGTH_SHORT).show()
                finish()
                showLoading(true)
            }else{
                Toast.makeText(this, "Failed to created", Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun deleteUser(id: Int) {
        myUserFormViewModel.deleteUser(id).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null){
                Toast.makeText(this, "Deleted Successfully", Toast.LENGTH_SHORT).show()
                if (isFavorite) removeFromFavorite()
                finish()
            }else{
                Toast.makeText(this, "Failed to delete", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun updateUser(id: Int, userData: UserDataResponse) {
        showLoading(false)
        myUserFormViewModel.updateUser(id, userData).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null){
                Toast.makeText(this, "Updated Successfully", Toast.LENGTH_SHORT).show()

                showLoading(true)
                updateFavoriteUser(userDataResponse)
                finish()
            }else{
                Toast.makeText(this, "Failed to update", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun updateFavoriteUser(userDataResponse: UserDataResponse) {
        if (favoriteUserId != null){
            favoriteViewModel.updateUser(
                FavoriteUser(
                    favoriteUserId as Long,
                    userDataResponse.address,
                    userDataResponse.id.toString(),
                    userDataResponse.name,
                    userDataResponse.phone
                )
            )
        }
    }


    private fun showLoading(state: Boolean) {
        if (state){
            pbMyUserFormLoading.visibility = View.VISIBLE
        }else{
            pbMyUserFormLoading.visibility = View.GONE
        }
    }

    private fun getUser(userId: Int){
        myUserFormViewModel.getUser(userId).observe(this, Observer { userDataResponse ->
            if (userDataResponse != null){
                Toast.makeText(this, "User loaded successfully", Toast.LENGTH_SHORT).show()
                populateFormData(userDataResponse)
                pbMyUserFormLoading.visibility = View.GONE
                llMyUserFormContent.visibility = View.VISIBLE
                setFavorite()
            }else{
                Toast.makeText(this, "Failed to load user", Toast.LENGTH_SHORT).show()
                pbMyUserFormLoading.visibility = View.GONE
            }
        })
    }

    private fun setFavorite() {
        if (userId != null){
            favoriteViewModel.getUser(userId as Int).observe(this, Observer {
                favoriteUser ->
                Log.d("masram", "getUser $favoriteUser")
                isFavorite = favoriteUser != null
                setFavoriteIcon()

                if (favoriteUser != null){
                    favoriteUserId = favoriteUser.favUserId
                }
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_my_user_favorite, menu)
        menuItem = menu
        setFavoriteIcon()

        if (!isEditUser){
            menu.findItem(R.id.menu_favorite).isVisible = false
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.menu_favorite -> {

                if (llMyUserFormContent.visibility == View.GONE){
                    Toast.makeText(this, "Can't add to favorite", Toast.LENGTH_SHORT).show()
                    return false
                }

                isFavorite = !isFavorite
                setFavoriteIcon()
                addOrRemoveFavorite()
                true
            }
            else -> true
        }
    }

    private fun addOrRemoveFavorite() {
        if (isFavorite){
            addToFavorite()
        }else{
            removeFromFavorite()
        }

        favoriteViewModel.getAllFavoriteUser().observe(this, Observer { listFavUser ->
            if (listFavUser.isNotEmpty()){
                for (i in 0 until listFavUser.size){
                    Log.d("masram", "" + listFavUser[i].favUserId +":" + listFavUser[i].userName)
                }
            }
        })
    }

    private fun removeFromFavorite() {
        if (userId != null){
            favoriteViewModel.deleteUser(userId as Int)
        }
        favoriteUserId = null
    }

    private fun addToFavorite() {
        favoriteViewModel.insertUser(
            FavoriteUser(0,
                etUserFormAddress.text.toString(),
                userId.toString(),
                etUserFormName.text.toString(),
                etUserFormHp.text.toString()
                )
        )
    }

    private fun setFavoriteIcon(){
        if (isFavorite){
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_bookmark_black )
            Toast.makeText(this, "Add to Favorite", Toast.LENGTH_SHORT).show()
        }
        else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_bookmark_border)
            Toast.makeText(this, "Remove From Favorite", Toast.LENGTH_SHORT).show()
        }
    }
}
