package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_classification.*

class ClassificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_classification)

        etClassificationNilai.setText("0")
        btnClassificationShow.setOnClickListener {
            val nilai = etClassificationNilai.text
            if (nilai.isEmpty()){
                Toast.makeText(this,"eh kosong",Toast.LENGTH_SHORT).show()
            }else{
                doClassification(nilai.toString().toInt())
            }

        }
    }

    fun doClassification(nilai: Int) {
        if (nilai>1000) {
            Toast.makeText(this, "nilai tidak boleh lebih dari 1000 cuy",Toast.LENGTH_SHORT).show()
        }
        tvClassificationText.text = when (nilai) {
            in 0..70 -> "lulus kurang"
            in 71..80 -> "lulus mepet"
            in 81..100 -> "ih wow"
            else -> {
                "ih error"
            }
        }

    }
}

