package id.co.iconpln.controlflowapp.hero

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.Hero
import id.co.iconpln.controlflowapp.model.HeroesData
import kotlinx.android.synthetic.main.activity_grid_hero.*
import kotlinx.android.synthetic.main.activity_list_hero.*

class GridHeroActivity : AppCompatActivity() {

    private var listHero: ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_hero)

        setupGridHero()
        showRecycleGrid()
        setupDividerGrid()
    }

    private fun showRecycleGrid() {
        rvGridHero.layoutManager = GridLayoutManager(this, 2)
        val gridHeroAdapter = GridHeroAdapter(listHero)
        rvGridHero.adapter = gridHeroAdapter

        gridHeroAdapter.setOnItemGridCallBack(object : GridHeroAdapter.OnItemClickCallBack{
            override fun onItemClick(hero: Hero) {
                Toast.makeText(this@GridHeroActivity, hero.name, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun setupGridHero() {
        rvGridHero.setHasFixedSize(true)
        listHero.addAll(HeroesData.listDatHero)
    }

    private fun setupDividerGrid(){
        val dividerItemDecoration = DividerItemDecoration(rvGridHero.context, DividerItemDecoration.HORIZONTAL)
        rvGridHero.addItemDecoration(dividerItemDecoration)
    }
}
