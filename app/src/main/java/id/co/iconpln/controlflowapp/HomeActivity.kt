package id.co.iconpln.controlflowapp

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import id.co.iconpln.controlflowapp.backgroundThread.BackgroundThreadActivity
import id.co.iconpln.controlflowapp.bottomSheetDialog.BottomSheetActivity
import id.co.iconpln.controlflowapp.contact.ContactActivity
import id.co.iconpln.controlflowapp.contactFragment.ContactTabActivity
import id.co.iconpln.controlflowapp.fragmentBottomNav.BottomNavActivity
import id.co.iconpln.controlflowapp.fragmentNavDrawer.NavDrawerActivity
import id.co.iconpln.controlflowapp.fragmentPagerTab.TabPagerActivity
import id.co.iconpln.controlflowapp.fragmentTab.TabActivity
import id.co.iconpln.controlflowapp.fragmentViewPager.ScrollActivity
import id.co.iconpln.controlflowapp.fragments.DemoFragmentActivity
import id.co.iconpln.controlflowapp.hero.GridHeroActivity
import id.co.iconpln.controlflowapp.hero.ListHeroActivity
import id.co.iconpln.controlflowapp.myContact.MyContactActivity
import id.co.iconpln.controlflowapp.myProfileFirst.MyProfileActivity
import id.co.iconpln.controlflowapp.myProfileLogin.MyProfileLoginActivity
import id.co.iconpln.controlflowapp.myUser.MyUserActivity
import id.co.iconpln.controlflowapp.sharedPreferences.SharedPreferencesActivity
import id.co.iconpln.controlflowapp.weather.WeatherActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), View.OnClickListener {

    private var doubleBackToExit = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setOnClickButton()

    }

    private fun setOnClickButton() {
        btnCalculation.setOnClickListener(this)
        btnClassification.setOnClickListener(this)
        btnLoginPage.setOnClickListener(this)
        btnCalculatePage.setOnClickListener(this)
        btnStylePage.setOnClickListener(this)
        btnDemoPage.setOnClickListener(this)
        btnVolumePage.setOnClickListener(this)
        btnIntentPage.setOnClickListener(this)
        btnComplexPage.setOnClickListener(this)
        btnConstraintPage.setOnClickListener(this)
        btnListHeroPage.setOnClickListener(this)
        btnGridHeroPage.setOnClickListener(this)
        btnDemoFragmentPage.setOnClickListener(this)
        btnDemoTabPage.setOnClickListener(this)
        btnDemoTabFragmentPage.setOnClickListener(this)
        btnBottomNav.setOnClickListener(this)
        btnNavDrawerPage.setOnClickListener(this)
        btnBottomSheetPage.setOnClickListener(this)
        btnLocalizationPage.setOnClickListener(this)
        btnScrollPage.setOnClickListener(this)
        btnSharedPreferencesPage.setOnClickListener(this)
        btnWeatherPage.setOnClickListener(this)
        btnContactPage.setOnClickListener(this)
        btnBackgroundPage.setOnClickListener(this)
        btnContactFragmentPage.setOnClickListener(this)
        btnMyContactPage.setOnClickListener(this)
        btnMyUserPage.setOnClickListener(this)
        btnMyProfileLoginPage.setOnClickListener(this)

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnCalculation -> {
                val calculationIntent = Intent(this, MainActivity::class.java)
                startActivity(calculationIntent)
            }
            R.id.btnClassification -> {
                val classficationIntent = Intent (this, ClassificationActivity::class.java)
                startActivity(classficationIntent)
            }
            R.id.btnLoginPage -> {
                val classficationIntent = Intent (this, LoginActivity::class.java)
                startActivity(classficationIntent)
            }
            R.id.btnCalculatePage -> {
                val classficationIntent = Intent (this, OperationActivity::class.java)
                startActivity(classficationIntent)
            }
            R.id.btnStylePage ->{
                val styleIntent = Intent(this, StyleActivity::class.java)
                startActivity(styleIntent)
            }
            R.id.btnDemoPage ->{
                val demoIntent = Intent(this, DemoActivity::class.java)
                startActivity(demoIntent)
            }
            R.id.btnVolumePage->{
                val volumeIntent = Intent(this, VolumeActivity::class.java)
                startActivity(volumeIntent)
            }
            R.id.btnIntentPage->{
                val intentIntent = Intent(this, IntentActivity::class.java)
                startActivity(intentIntent)
            }
            R.id.btnComplexPage -> {
                val complexIntent = Intent(this, ComplexActivity::class.java)
                startActivity(complexIntent)
            }
            R.id.btnConstraintPage ->{
                val constraintIntent = Intent(this, ConstraintActivity::class.java )
                startActivity(constraintIntent)
            }
            R.id.btnListHeroPage ->{
                val listHeroIntent = Intent(this, ListHeroActivity::class.java)
                startActivity(listHeroIntent)
            }
            R.id.btnGridHeroPage ->{
                val gridHeroIntent = Intent(this, GridHeroActivity::class.java)
                startActivity(gridHeroIntent)
            }
            R.id.btnDemoFragmentPage -> {
                val demoFragmentIntent = Intent(this, DemoFragmentActivity::class.java)
                startActivity(demoFragmentIntent)
            }
            R.id.btnDemoTabPage -> {
                val demoTabPage = Intent(this, TabActivity::class.java)
                startActivity(demoTabPage)
            }
            R.id.btnDemoTabFragmentPage -> {
                val tabFragmentTab = Intent(this, TabPagerActivity::class.java)
                startActivity(tabFragmentTab)
            }
            R.id.btnBottomNav ->{
                val bottomNav = Intent(this, BottomNavActivity::class.java)
                startActivity(bottomNav)
            }
            R.id.btnNavDrawerPage -> {
                val navDrawerIntent = Intent(this, NavDrawerActivity::class.java)
                startActivity(navDrawerIntent)
            }
            R.id.btnBottomSheetPage -> {
                val bottomSheetIntent = Intent(this, BottomSheetActivity::class.java)
                startActivity(bottomSheetIntent)
            }
            R.id.btnLocalizationPage -> {
                val localizationIntent = Intent(this, LocalizationActivity::class.java)
                startActivity(localizationIntent)
            }
            R.id.btnScrollPage -> {
                val scrollIntent = Intent(this, ScrollActivity::class.java)
                startActivity(scrollIntent)
            }
            R.id.btnSharedPreferencesPage -> {
                val sharedPreferencesIntent = Intent(this, SharedPreferencesActivity::class.java)
                startActivity(sharedPreferencesIntent)
            }
            R.id.btnWeatherPage -> {
                val weatherIntent = Intent(this, WeatherActivity::class.java)
                startActivity(weatherIntent)
            }
            R.id.btnContactPage -> {
                val contactIntent = Intent(this, ContactActivity::class.java)
                startActivity(contactIntent)
            }
            R.id.btnBackgroundPage -> {
                val backgroundIntent = Intent(this, BackgroundThreadActivity::class.java)
                startActivity(backgroundIntent)
            }
            R.id.btnContactFragmentPage -> {
                val contactFragmentIntent = Intent(this, ContactTabActivity::class.java)
                startActivity(contactFragmentIntent)
            }
            R.id.btnMyContactPage -> {
                val myContactIntent = Intent(this, MyContactActivity::class.java)
                startActivity(myContactIntent)
            }
            R.id.btnMyUserPage -> {
                val myUserIntent = Intent(this, MyUserActivity::class.java)
                startActivity(myUserIntent)
            }
            R.id.btnMyProfileLoginPage -> {
                val myProfileLogin = Intent(this, MyProfileActivity::class.java)
                startActivity(myProfileLogin)
            }

        }
    }

    override fun onBackPressed() {
        if (doubleBackToExit) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExit = true
        Toast.makeText(this, "press back again to leave", Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable { doubleBackToExit = false }, 1000)
    }

}

