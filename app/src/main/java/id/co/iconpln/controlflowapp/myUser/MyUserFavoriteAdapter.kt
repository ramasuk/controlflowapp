package id.co.iconpln.controlflowapp.myUser

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import kotlinx.android.synthetic.main.item_list_my_user.view.*

class MyUserFavoriteAdapter: RecyclerView.Adapter<MyUserFavoriteAdapter.MyFavoriteUserViewHolder>() {

    private var userData = emptyList<FavoriteUser>()
    private lateinit var onItemClickCallBack: OnItemClickCallBack

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyFavoriteUserViewHolder {
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_list_my_user, parent, false)
        return MyFavoriteUserViewHolder((mView))
    }

    override fun getItemCount(): Int {
        return userData.size
    }

    override fun onBindViewHolder(holder: MyFavoriteUserViewHolder, position: Int) {
        holder.bind(userData[position])

        holder.itemView.setOnClickListener {
            onItemClickCallBack.onItemClick(userData[holder.adapterPosition])
        }
    }


    fun setData(myUserItem: List<FavoriteUser>){
        val listFavUser = ArrayList<FavoriteUser>()
        for (i in 0 until myUserItem.size){
            listFavUser.add(myUserItem[i])
        }
        userData = listFavUser
        notifyDataSetChanged()
    }

    inner class MyFavoriteUserViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(myUserItem : FavoriteUser){
            itemView.tvUserName.text = myUserItem.userName
            itemView.tvUserAdress.text = myUserItem.userAddress
            itemView.tvUserMobile.text = myUserItem.userPhone
//            itemView.setOnClickListener {
//                onItemClickCallBack.onItemClick(myUserItem)
//            }
        }
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallBack){
        this.onItemClickCallBack = onItemClickCallback
    }

    interface OnItemClickCallBack {
        fun onItemClick(myUser: FavoriteUser)
    }

}