package id.co.iconpln.controlflowapp.sharedPreferences

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_shared_preferences.*

class SharedPreferencesActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var userPreference: UserPreference
    private lateinit var user: User

    companion object{
        private const val REQUEST_CODE = 100
    }

    private var isPreferenceEmpty = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shared_preferences)

        btnSharedPreferences()

        supportActionBar?.title = "My user preference"
        userPreference = UserPreference(this)
        showExistingPreference()
    }

    private fun showExistingPreference() {
        user = userPreference.getUser()
        populateView(user)
        checkForm(user)
    }

    private fun checkForm(user: User) {
        when {
            user.name.toString().isNotEmpty() -> {
                btnPrefSave.text = resources.getText(R.string.sp_change)
                isPreferenceEmpty = false
            }else -> {
            btnPrefSave.text = resources.getText(R.string.sp_save)
                isPreferenceEmpty = true
        }

        }
    }

    private fun populateView(user: User) {
        tvPrefName.text = if (user.name.toString().isEmpty())
            "Tidak ada" else user.name
        tvPrefAge.text = if (user.age.toString().isEmpty())
            "Tidak ada" else user.age.toString()
        tvPrefEmail.text = if (user.email.toString().isEmpty())
            "Tidak ada" else user.email
        tvPrefHandphone.text = if (user.handphone.toString().isEmpty())
            "Tidak ada" else user.handphone
        tvPrefHobby.text = if (!user.hasReadingHobby)
            "Tidak membaca" else "membaca"
    }

    private fun btnSharedPreferences() {
        btnPrefSave.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btnPrefSave -> {
                val prefSaveIntent = Intent(this, SharedPreferencesFormActivity::class.java)


                when {
                    isPreferenceEmpty -> {
                        prefSaveIntent.putExtra(
                            SharedPreferencesFormActivity.EXTRA_TYPE_FORM,
                            SharedPreferencesFormActivity.TYPE_ADD
                        )
                    }
                    else -> {
                        prefSaveIntent.putExtra(
                            SharedPreferencesFormActivity.EXTRA_TYPE_FORM,
                            SharedPreferencesFormActivity.TYPE_EDIT
                        )
                    }
                }
                prefSaveIntent.putExtra("USER", user)
                startActivityForResult(prefSaveIntent, REQUEST_CODE)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE){
            if (resultCode == SharedPreferencesFormActivity.RESULT_CODE){
                user = data?.getParcelableExtra(SharedPreferencesFormActivity.EXTRA_RESULT) as User
                populateView(user)
                checkForm(user)
            }
        }
    }
}
