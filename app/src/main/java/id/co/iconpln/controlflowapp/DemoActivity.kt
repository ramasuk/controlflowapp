package id.co.iconpln.controlflowapp

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_demo.*

class DemoActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)

        setOnClickListener()


    }

    private fun setOnClickListener() {
        btnSubmit.setOnClickListener(this)
        btnSnackbar.setOnClickListener(this)
        btnSnackBarButton.setOnClickListener(this)
        btnSnackBarCustom.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when(v.id) {
            R.id.btnSubmit ->{
                val styleIntent  = Intent(this, StyleActivity::class.java)
                startActivity(styleIntent)
            }
            R.id.btnSnackbar -> {
                Snackbar.make(clDemo,"This is snackbar", Snackbar.LENGTH_SHORT).show()

            }
            R.id.btnSnackBarButton -> {
                Snackbar.make(clDemo, "Message is deleted", Snackbar.LENGTH_SHORT).setAction("undo", undoListener).show()
            }
            R.id.btnSnackBarCustom -> {
                val customSnackbar = Snackbar
                    .make(clDemo, "This is snackbar custom", Snackbar.LENGTH_LONG)
                    .setAction("Undo", undoListener)
                    .setActionTextColor(ContextCompat.getColor(this, R.color.button_snackbar))

                val snackbarView = customSnackbar.view
                val textSnackBar: TextView = snackbarView.findViewById(R.id.snackbar_text)
                textSnackBar.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
                textSnackBar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)

                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))

                customSnackbar.show()

            }

        }
    }

    private val undoListener = object : View.OnClickListener{
        override fun onClick(view: View?) {
            Snackbar.make(clDemo, "Message is restored", Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_demo, menu)
        setupSearchMenu(menu)

        return super.onCreateOptionsMenu(menu)
    }

    private fun setupSearchMenu(menu: Menu?) {
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu?.findItem(R.id.action_demo_search)?.actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.queryHint = resources.getString(R.string.search_hint)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                Toast.makeText(this@DemoActivity, query, Toast.LENGTH_SHORT).show()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }

    override fun onStart() {
        super.onStart()
        Log.d("demo", "start onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d("demo", "start onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("demo", "start onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d("demo", "start onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("demo", "start onDestroy")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("demo", "start onRestart")
    }




}
